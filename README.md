# repohomewebjavascript

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to JavaScript

**here are the repos**

I have written some code related to javascript. I mostly use it for training and also with web mvc.

---

1. [https://bitbucket.org/thechalakas/using_javascript]

2. [https://bitbucket.org/thechalakas/array_objects]

3. [https://bitbucket.org/thechalakas/closures]

4. [https://bitbucket.org/thechalakas/cookies]

5. [https://bitbucket.org/thechalakas/dom_navigation]

6. [https://bitbucket.org/thechalakas/event_listener]

7. [https://bitbucket.org/thechalakas/events]

8. [https://bitbucket.org/thechalakas/functions]

9. [https://bitbucket.org/thechalakas/grabbing_dom_elements]

10. [https://bitbucket.org/thechalakas/inputpractice]

11. [https://bitbucket.org/thechalakas/json]

12. [https://bitbucket.org/thechalakas/manipulating_dom_elements]

13. [https://bitbucket.org/thechalakas/objects]

14. [https://bitbucket.org/thechalakas/scripts]

15. [https://bitbucket.org/thechalakas/syntax]

---

Others

1. [UTL training - basic usage of web - HTML, CSS, JS and consuming JSON API SERVICE](https://bitbucket.org/thechalakas/webandcloud)
2. [UTL training - basic bootstrap usage](https://bitbucket.org/thechalakas/pbs1)
3. [Bootstrap basics with notes - IIHT](https://bitbucket.org/thechalakas/iihtbootstrap)

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 